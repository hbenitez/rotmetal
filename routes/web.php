<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('index', function () {
    return view('index');
})->name('index');
Route::get('crew', function () {
    return view('crew');
})->name('crew');;
Route::get('contact', function () {
    return view('contact');
})->name('contact');



