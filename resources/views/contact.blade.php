@extends('layout.main')

@section('meta')
    <title>ROT Contacto</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('mainFeature')

@stop
@section('content')
    <div id="index">
        <section class="container-fluid">
            <div class="container main">
                <div class="col-sm-4 gigs">
                    <div>
                        <h1>Contacto</h1>
                        <h2>Tel: 444 200 2417</h2>
                        <p>sidknot@gmail.com</p>
                        <p>https://www.facebook.com/ROTMETAL</p>
                    </div>
                </div>
                <div class="col-sm-8 main-img">
                    <img src="{{asset('images/rot-redstripe-bkbg.jpg')}}" class="img-responsive">
                </div>


            </div>
        </section>

        <section class="container-fluid promo">
            <div class="container">
                <img src="{{asset('images/rot-sales.jpg')}}" class="img-responsive">
            </div>
        </section>









    </div>
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop