@extends('layout.main')

@section('meta')
    <title>ROT Home</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('mainFeature')

@stop
@section('content')
    <div id="index">
        <section class="container-fluid">
            <div class="container main">
                <div class="col-sm-8 main-img">
                    <img src="{{asset('images/rotband.jpg')}}" class="img-responsive">
                </div>
                <div class="col-sm-4 gigs">
                    <h1>GIGS</h1>
                    <hr>
                    <div class=" col-xs-12 main-txt">
                        <h4>La Hermandad del Metal y la cerveza</h4>

                        <div class="col-xs-7">
                            <p>S/N</p>
                            <p>Charcas, S.L.P.</p>
                            <p><span>Rockeado</span></p>
                        </div>
                        <div class="col-xs-5">
                            <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/hermandadelmetal/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-xs-12 main-txt">
                        <h4>Billar El Alfil Negro</h4>
                        <div class="col-xs-7">

                            <p>Ricardo B.Anaya #1418</p>
                            <p>Col.Providencia, S.L.P.</p>
                            <p><span>Rockeado</span></p>
                        </div>
                        <div class="col-xs-5">
                            <a href="https://goo.gl/maps/mzyCC9k7Yk42"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/alfilnegrobanaya/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-xs-12 main-txt">
                        <h4>Chente´s Bar</h4>
                        <div class="col-xs-7">

                            <p>Sirconio #825</p>
                            <p>Valle Dorado, S.L.P.</p>
                            <p><span>03/12/16 - 20:30 hrs</span></p>
                        </div>
                        <div class="col-xs-5">
                            <a href="https://goo.gl/maps/pVuMC5icgis"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/ChentesSnackBar"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="container-fluid content">
            <div class="container">

                    <div class="col-sm-6">
                        <img src="{{asset('images/rotband-crew.jpg')}}" class="img-responsive">
                        <p>Somos una banda potosina enfocada en el género Brutal Death Metal, acualmente estamos enfocados en nuestro nuevo disco lleno de podredumbre y poder. Te invitamos a seguirnos y apoyarnos para un buen rato de metal podrido.</p>
                    </div>
                    <div class="col-sm-3">
                        <div class="news">
                            <h2>Rot News</h2>
                            <hr>
                            <h4>Satánico pandemonium</h4>
                            <p>Hemos sido invitados a tocar con la banda italiana <a href="https://www.facebook.com/2MinutaDrekaOfficial/">2 Minuta Dreka</a> en el <a href="https://www.facebook.com/ChentesSnackBar">Chentes Bar</a> el día 3 de diciembre junto con otras grandes bandas, puedes consultar el evento en facebook <a href="https://www.facebook.com/events/1663699980625782/">"Satanico Pandemonum" tour Mexxxican</a>, vengan listos a calentar garganta con cheves y metal podrido.</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <iframe src="https://embed.spotify.com/?uri=spotify%3Auser%3Asidknot%3Aplaylist%3A7k2W01dTxkEKpscBQ2tekR" width="260" height="380" frameborder="0" allowtransparency="true"></iframe>
                    </div>
            </div>
        </section>
        <section class="container-fluid promo">
            <div class="container">
                <img src="{{asset('images/rot-sales.jpg')}}" class="img-responsive">
            </div>
        </section>









    </div>
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop